// (c) 2010 Naked on Pluto 
// AGPLv3
//
// Pluto Teaser.

// init with our app id
FB.init({appId: '53a91856ec65f1ab6a4c84de789c2670', status: true, 
         cookie: true, xfbml: true});

// add some html to the page
function print(id, html)
{ 
    var div=document.createElement("div");   
    div.id = id;
    div.innerHTML = html;
    document.getElementById('werp').appendChild(div);
}

// clear the page
function clear()
{
    var element=document.getElementById('werp');
    while (element.firstChild) {
        element.removeChild(element.firstChild);
    }
}

// converts a js object into text for displaying
function object_to_string(obj,depth)
{
    text="";
    for (i in obj)
    {
        if (typeof(obj[i])=="object")
        {            
            text+="<b>"+i+"</b>"+":"+object_to_string(obj[i],depth+1);
        }      
        else
        {
            text+=i+":"+obj[i]+"<br />";
        }
    }
    return text;
}

// Pick a random item from list
function array_rnd(array)
{
    return array[Math.floor(array.length*Math.random())];
}

/////////////////////////////////////////////////////////////////
// the login stuff

// do the login onto facebook
function login()
{
    FB.login(function(response) {
        if (response.session) {
            clear(); // remove the login button
            start();
        } else {
            print('foo', ":( the login didn't work!");
        }
    });
}

// check to see if the user has logged in, and ask for login if they haven't
function check_login()
{
    FB.getLoginStatus(function(response) {
        if (response.session) {       
            // we are already logged in
            start();
        } else {
            // the login is a popup so need the user to click on a button
            // for the browser to allow this to happen
            print('dialog', '<center><input type="button" value="Read" onClick="login();"></center>');
        }
    });
}

////////////////////////////////////////////////////////////////
// actually do the stuff we want to do

function start()
{
    // 2 eurocents bot vocabulary
    // var greets = ["Hi, ","Hello, ","Greetings, ", "Howdy, ", "Hey, ", "Sup, "];
    var intro = ["So, ","Then, ","In the end, ", "But, ", "Besides, "]
    var question = ["can we agree on that?", "see what I mean?", "you get it?", "that's not possible, right?", "it's a bit weird this stuff, no?", "is it all fake?"];
    var reply = ["lol","Sure.","You bet.","Probably not.","You think so?","No, not really.","Definitively."];
    var quiet = ["Shhhh, ","Quiet, ","Fuck, ","Crap, ","Ooops, ", "Damn, "];
    var action = [" heard "," saw "];
    var who = ["you.","me.","us."];
    var object = ["banana tree", "unicorn", "hypercube", "model of the universe", "bottle of vodka"];
    var exit = [" run away inside a cavern.", " jump into a hole.", " fly away on pink balloon.", " disappear behind a smoke screen.", " dissolve into the ether."];

    // Paranoia
    FB.api('/me/friends', function(response) {
        var myFriends = [];
        for (i in response.data) {
                myFriends.push(response.data[i].name);
        }
        var friend_1 = array_rnd(myFriends);
        var friend_2 = array_rnd(myFriends);
        
        print('narrative', 
              'It does not take you much time to recognize this familiar sound. A sound that you thought ' +
              'you would never hear again since your arrival on Pluto: human voices.');
        print('narrative',
              'As you walk closer towards the source, you suddenly see ' + friend_1 + ' and ' + friend_2 + 
              '. They are whispering and ' + friend_1.split(' ')[0] + ' seems to be holding a miniature ' + 
               array_rnd(object) + ', obviously upset about something:');

        print('dialog', '- '+ array_rnd(intro) + array_rnd(question));
        print('dialog', '- '+ array_rnd(reply));
        FB.api('/me', function(response) {
                print('dialog', '- ' + array_rnd(quiet) + 'I think ' + response.first_name + array_rnd(action) + array_rnd(who));
                print('narrative', 'Before you get a chance to say something, ' + friend_1.split(' ')[0] + 
                      ' and ' + friend_2.split(' ')[0] + array_rnd(exit));
                print('narrative', '<em>Stay tuned for more...<em>');
        });
    });
}

///////////////////////////////////////////////////////////

// start the process going!
check_login();
